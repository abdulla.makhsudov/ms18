package az.ingress.demo.configuration;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class Human {
    private final People people;

    @Lazy
    public Human(People people) {
        this.people = people;
    }
}
