package az.ingress.demo.service;

import az.ingress.demo.model.Student;


public interface StudentService {
    Student getStudentById(int id);

    void save(Student student);
    Integer updateStudent(int id, Student student);
}
