package az.ingress.demo.configuration;

import az.ingress.demo.model.Student;

public class Test {

    public static int getVal() {
        return 100;
    }

    public static void main(String args[]) {
//        ifelse();
//        replaceMethod();
//        stringequals();

//        switchCase();

        StringBuilder stringBuilder = new StringBuilder("abc");
        stringBuilder.deleteCharAt(0);
        stringBuilder.delete(1,2);
        System.out.println(stringBuilder);

    }

    private static void switchCase() {
        int num = 10;
        final int num2 = 20;
        switch (num) {
            case 10*3:
                System.out.println(1);
                break;
            default:
                System.out.println("default");
        }
    }

    private static void stringequals() {
        String s1 = new String("Java");
        String s2 = new String("Java");
        String s3 = "Java";
        String s4 = "Java";

        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s3 == s4);

        do {
            System.out.println(s1.equals(s2));
        } while (s3 == s4);
    }

    private static void replaceMethod() {
        String s = "java2s";
        s.replace('a', 'Z').trim().concat("Aa");
        s.substring(0, 2);
        System.out.println(s);
    }

    private static void ifelse() {
        boolean b = false;
        if (b == true)
            for (int i = 0; i < 2; i++)
                System.out.println(i);
        else
          System.out.println("else");
    }
}
