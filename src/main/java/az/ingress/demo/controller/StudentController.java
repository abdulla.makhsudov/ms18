package az.ingress.demo.controller;

import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {


    private final ModelMapper modelMapper;

    @Autowired
    private StudentService studentService;

    @GetMapping("{id}")
    public Student getStudentById(@PathVariable int id) {
        log.info("Request accepted: {} - {}", id, 5);
        return studentService.getStudentById(id);
    }

    @PostMapping
    public String saveStudent(@RequestBody Student student) {
        studentService.save(student);
        return "Student insert to db with name :" + student.getName();
    }

    @PutMapping("/{id}")
    public Integer updateStudent(@RequestBody Student student, @PathVariable int id) {
        return studentService.updateStudent(id, student);
    }

    @DeleteMapping
    public String deleteStudent(@RequestParam(value = "id") int id) {
        return "deleted student with id : " + id;
    }

}
