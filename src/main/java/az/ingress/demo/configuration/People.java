package az.ingress.demo.configuration;

import org.springframework.stereotype.Component;

@Component
public class People {
    private final Human human;

    public People(Human human) {
        this.human = human;
    }
}
