package az.ingress.demo.configuration;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("student")
@Data
public class Properties {
    List<String> name;
    String surname;
    int age;
}
