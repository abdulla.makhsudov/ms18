package az.ingress.demo;

import az.ingress.demo.configuration.Properties;
import az.ingress.demo.controller.StudentController;
import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class DemoApplication implements CommandLineRunner {

    // Http response code
    //RequestMapping()
    //PostMapping -> RequestBody, RequestParam, RequestHeader
    //PutMapping
    //DeleteMapping
    //H2 database
    //package
    //instance -> inject-> annotation
    //remove service annotation application failed to upp 0 constructor
    //Configuration, ModelMapper

    //CommandLinner
    //why interface
    //Qualifier -> field level bean name or impl class name inject
    //primary -> class level
    //singleton
    //prototype
    //Builder pattern
    //application property, yaml -> server port
    //@Value
    //@ConfigurationProperty and Configuration
    //@Lazy -> constructor
    //docker
    //docker exec -it con id bash = /bin/sh
    //docker compose
    //app connect to mysql docker
    //java -jar
    //Dockerfile
    //hostname
    //docker compose create same network
    //docker build . -t ms18:v1
    //docker runtime and build time command
    //CMD vs ENTRYPOINT cmd -> should be change
    //docker tag local-image(myapp):tagname(v1) new-repo(abdulla91/ms18):tagname(v1)
    //docker push new-repo(abdulla91/ms18):tagname(v1)
    //liquibase -> yaml
    //db.changelog -> master db
    //database changelog and log lock
    //manual log lock to true
    //md5sum change another change set or file


    private final StudentService studentServiceImpl;
    private final StudentController studentController;
    private final ModelMapper modelMapper;
    private final Properties properties;

    //    @Value("${student.name}")
    private String name;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) {
        studentServiceImpl.save(new Student(1, "Senan-4", "Xudulu", 27));
        log.error("Student name is : {}", name);
        for (String name : properties.getName()) {
            System.out.println(name);
        }

        System.err.println("Age :" + properties.getAge());
        System.err.println("Surname :" + properties.getSurname());
    }
}
