FROM openjdk:17
COPY build/libs/demo-0.0.1-SNAPSHOT.jar /ms18-app/
ENTRYPOINT ["java"]
CMD ["-jar", "/ms18-app/demo-0.0.1-SNAPSHOT.jar"]